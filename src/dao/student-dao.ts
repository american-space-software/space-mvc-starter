import { dao, MetastoreService } from "space-mvc";
import { Student } from "../dto/student";

@dao()
class StudentDao {

    private get store() {
        return this.metastoreService.getStore(this.metastoreService.WALLET_METASTORE, "student") 
    }

    constructor(
        private metastoreService:MetastoreService
    ) {}

    async put(key:string, student:Student) {
        return this.store.put(key, student)
    }

    async get(key:string): Promise<Student> {
        return this.store.get(key)
    }

    async list(offset?: number, limit?: number) : Promise<Student[]> {
        return this.store.list(offset, limit)
    }

}

export {
    StudentDao
}