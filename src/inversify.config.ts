import { Container } from "space-mvc"
import { HomeController } from "./controller/home-controller"
import { DownloadService } from "./service/download-service"

const container = new Container()

//Bind our home controller
container.bind(HomeController).toSelf().inSingletonScope()

//Bind the service and DAO for person
container.bind(DownloadService).toSelf().inSingletonScope()

export {
    container
}