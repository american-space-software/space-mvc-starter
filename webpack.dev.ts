import { merge } from 'webpack-merge'
import path from 'path'
import common from './webpack.common'

export default merge(common, {
    //@ts-ignore
    mode: 'development',
    devtool: 'source-map',
    //@ts-ignore
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        compress: true,
        port: 8080,
        injectClient: false
    }
})