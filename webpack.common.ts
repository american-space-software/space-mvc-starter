import path from 'path'
import webpack from 'webpack'


const HtmlWebpackPlugin = require('html-webpack-plugin')
const WebpackPwaManifest = require('webpack-pwa-manifest')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const fileLoader = {
  loader: 'file-loader',
  options: {
    name: '[folder]/[name].[ext]'
  }
}


const babelLoader = {
  loader: 'babel-loader',
  options: {
    cacheDirectory: false,
    presets: ['@babel/preset-env']
  }
}

const framework7ComponentLoader = {
  loader: 'framework7-loader',
  options: {
    helpersPath: './src/template7-helpers-list.js',
    partialsPath: './src/pages/',
    partialsExt: '.f7p.html'
  }
}

export default {
  entry: './src/index.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: 'ts-loader',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/,
        use: [fileLoader],
      },
      {
        test: /\.f7.html$/,
        use: [babelLoader, framework7ComponentLoader],
      }
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
    alias: {
      buffer: 'buffer'
    }
  },
  output: {
    filename: 'mlbc.js',
    library: "mlbc",
    path: path.resolve(__dirname, 'public')
  },
  plugins: [
    new CleanWebpackPlugin({
      dangerouslyAllowCleanPatternsOutsideProject: true
    }),
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    }),

    new HtmlWebpackPlugin({
      inject: false,
      title: 'MLBC Archive',
      favicon: 'src/html/favicon.ico',
      template: 'src/html/index.html',
      filename: 'index.html'
    }),

    new WebpackPwaManifest({
      name: 'MLBC Archive',
      short_name: 'mlbc-archive',
      description: 'MLBC Archive'
    })
  ]
}





