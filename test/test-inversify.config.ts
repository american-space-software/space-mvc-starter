import { Container, ethers, initTestContainer } from "space-mvc";

import { DownloadService } from "../src/service/download-service";

let container:Container 

export async function getTestContainer() {

    if (container) return container

    container = new Container()

    container.bind(DownloadService).toSelf().inSingletonScope()    

    let provider = ethers.getDefaultProvider("homestead")
    let wallet = await ethers.Wallet.createRandom(provider)

    return initTestContainer(container, provider, wallet, undefined, undefined)

}
