# Space MVC Starter

A starter SpaceMVC project. A basic template. 

[Live](https://american-space-software.gitlab.io/space-mvc-starter)



## Build

Development build. Creates a web server, builds, and watches for changes. 

```console
npm run start:dev 
```

Distribution build. Webpack populates the public folder with a compilied and minimized copy of the progressive web app. 

```console
npm run build
```

