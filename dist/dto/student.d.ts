interface Student {
    walletAddress?: string;
    firstName?: string;
    lastName?: string;
    displayName?: string;
}
export { Student };
